# VE-Snippets

| Prefix | Description                                          |
| ------ | ---------------------------------------------------- |
| tsxt   | Typescript React Template                            |
| irsc   | Import react with @emotion/styled                    |
| us     | useState hook from React                             |
| ue     | useEffect hook from React                            |
| enc    | Export named function component                      |
| encwp  | Export named function component with Props interface |
